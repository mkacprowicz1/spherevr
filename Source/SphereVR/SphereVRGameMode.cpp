// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SphereVRGameMode.h"
#include "SphereVRHUD.h"
#include "SphereVRCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASphereVRGameMode::ASphereVRGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASphereVRHUD::StaticClass();
}
