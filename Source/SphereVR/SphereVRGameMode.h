// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SphereVRGameMode.generated.h"

UCLASS(minimalapi)
class ASphereVRGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASphereVRGameMode();
};



